﻿function submitForm() {    
    var sDate1 = document.getElementById('iCurrentDate').value;
    var sDate2 = document.getElementById('iPreviousDate').value;
    if (checkForm(sDate1) && checkForm(sDate2)) {
        document.EntryForm.submit();
    }
    else {
        alert("Please fill in a valid date range to run this report");
    }
}

function checkForm(sDate1)
  {
    // regular expression to match required date format
    re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

    if(sDate1 != ' ' && !sDate1.match(re)) {
      return false;
    }

    return true;
  }

$(function () {
    $(".datepicker").datepicker({
        showOn: "button",
        buttonImage: "/CRM/Themes/img/color/Buttons/calCalendar.gif",
        buttonImageOnly: true,

        dateFormat: 'dd/mm/yy',
        maxDate: 0,
        changeYear: true,
        changeMonth: true,
        selectOtherMonths: true,
        maxDate: "+100m +100w",
        showOtherMonths: true,
        showButtonPanel: true
    });
});






