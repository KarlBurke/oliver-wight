﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using eWare;
using OfficeOpenXml;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using OfficeOpenXml.Style;
using System.Drawing;
using Oliver_Wight.Includes;
using Oliver_Wight.eWare;
using System.Collections;



namespace Oliver_Wight
{
    public partial class dotNetPage : System.Web.UI.Page
    {



        protected void Page_Load(object sender, EventArgs e)
        {
            #region Initialise Page
            eWareConnector eWareConn = new eWareConnector();
            eWareBase eWare = eWareConn.Init();
            Includes.EnbuUIFunctions EnbuUI = new Includes.EnbuUIFunctions(eWare);

            EnbuUIFunctions eUI = new EnbuUIFunctions(eWare);

            //Check for Form Data and convert to DateTime values from string
            String sCurrentDate = Request.Form["currentDate"];
            String sPreviousDate = Request.Form["previousDate"];
            String sRevisionCount = Request.Form["RevisionCount"];
            String sExportPath = Request.Form["ExportPath"];
            String sExportMethod = Request.Form["OutputMethod"];
            String sFormatOutput = Request.Form["formatSpreadSheet"];
            String sgroupBy = Request.Form["groupBy"];
            String sincludeNoChange = Request.Form["chkChangesOnly"];
            String sShowPreviousColumns = Request.Form["chkshowPrevious"];
            DateTime dcurrentDate = Convert.ToDateTime(sCurrentDate);
            DateTime dpreviousDate = Convert.ToDateTime(sPreviousDate);
            int revHistory = Convert.ToInt16(sRevisionCount);
            
            //Attach javascript/CSS to Page
            LiteralControl javascriptRef = new LiteralControl("<script type='text/javascript' src='Includes/EnbuUtils.js'></script>");
            LiteralControl styleSheetRef = new LiteralControl("<link rel='stylesheet' href='Includes/OppertunityStyle.css'>");
            Page.Header.Controls.Add(javascriptRef);
            Page.Header.Controls.Add(styleSheetRef);

            //Setup Container and buttons
            var container = eWare.GetBlock("container");
            var sSubmitButton = eWare.Button("Calculate", "continue.gif", "javascript:submitForm();");
            container.DisplayButton[1] = false;
            container.AddButton(sSubmitButton);

            //Create User Inputs
            ArrayList paramPanel = buildParameters(sCurrentDate, sPreviousDate, revHistory.ToString());
            String panel = EnbuUI.enbu_UI_draw_panel("Opportunity Progress Report", paramPanel, 2, "", "");
            var paramBlock = eWare.GetBlock("Content");
            paramBlock.Contents += panel;
            container.AddBlock(paramBlock);
            #endregion

            //handle Results if a Date Range has been Specified
            if (CheckDate(sPreviousDate) && CheckDate(sCurrentDate))
            {
                

                String groupByString = "userName";
                if (sgroupBy == "1")
                {
                    groupByString = "userName";
                }
                else if (sgroupBy == "2")
                {
                    groupByString = "comp_name";
                }
                else if (sgroupBy == "3")
                {
                    groupByString = "oppo_opportunityID";
                }

                //Setup table to hold all the relevant changes
                DataTable rawData = new DataTable();
                //build the SQL String for this - Get the opportunities that were In Progress at the time or are still in progress
                String sSQL = getRelevantOppsSQL(dpreviousDate, dcurrentDate);

                //Response.Write(sSQL);

                //Load DataTable
                rawData = SpreadSheet.SelectSQL(sSQL, eWare);

                DataView view = new DataView(rawData);
                DataTable distinctValues = view.ToTable(true, "oppo_opportunityid");

                //Response.Write(sSQL + "<br>");

                //Get List of OppID's from this to get the final results
                String oppIDList = "";
                foreach (DataRow dr in distinctValues.Rows)
                {
                    oppIDList += dr["oppo_opportunityid"].ToString() + ",";
                }
                //remove last comma
                oppIDList = oppIDList.Remove(oppIDList.Length - 1);
                
                //Get the max two dates of the relevent opportunities
                DataTable prevCurrentTable = new DataTable();
                String preCurrentSQL = getCurrentPreviousRecsSQL(oppIDList, dcurrentDate, dpreviousDate);
                
                //Response.Write(preCurrentSQL + "<br>");
                //Response.End();
                
                prevCurrentTable = SpreadSheet.SelectSQL(preCurrentSQL, eWare);

                
                                 
                //Specify Columns required
                SpreadSheet.FieldCollection fieldCollection = new SpreadSheet.FieldCollection();
                fieldCollection.InsertField("Oppo_AssignedUserId", "Assigned To", false);
                fieldCollection.InsertField("userName", "Assigned", false);
                fieldCollection.InsertField("comp_name", "Company", true);
                fieldCollection.InsertField("oppo_description", "Description", true);
                fieldCollection.InsertField("oppo_stage", "Stage", true, true);         //has previous values
                fieldCollection.InsertField("oppo_Status", "Status", false);
                fieldCollection.InsertField("oppo_Forecast", "SavedValue", false);
                fieldCollection.InsertField("FinalAmount", "Forecast (GBP)", true, true);
                fieldCollection.InsertField("oppo_createddate", "Last Updated", true, false);
                fieldCollection.InsertField("oppo_opportunityID", "OpportunityID", false);
                fieldCollection.InsertField("oppo_ProjectStartDate", "Project Start Date", true, true);
                fieldCollection.InsertField("Oppo_Opened", "Date Opened", true);
                fieldCollection.InsertField("oppo_opportunityprogressid", "ProgressID", false);
                

                DataBlock dB = DataBlock.createEnbuDataObject(prevCurrentTable, fieldCollection);
                dB.showColumnHeaders = true;
                dB.showPrevious = true;
                                
                SpreadSheet.EnbuReport eR = new SpreadSheet.EnbuReport();
                eR.CreateWorkSheet("Opportunity Progress Report");

                if (sincludeNoChange == "1")
                {
                    SpreadSheet.createGroupsBy(eR, dB, "Opportunity Progress Report", groupByString, true, true, false);
                }
                else
                {
                    SpreadSheet.createGroupsBy(eR, dB, "Opportunity Progress Report", groupByString, true, true, true);
                }
                
                                                                              
                //Decide how to process the results
                if (sExportMethod == "1")
                {
                    var HTMLOutput = eWare.GetBlock("Content");
                    HTMLOutput.Contents += "Result<br>" + eR.ExportToHTML(); // +"<br><br>" + sF.ConvertDataTableToHTML(reducedData, fieldCollection);
                    container.AddBlock(HTMLOutput);
                }
                else
                {
                    Boolean formatOutput = false;
                    if (sFormatOutput == "1")
                    {
                        formatOutput = true;
                    }

                    byte[] ep = eR.ExportToExcel("A1", formatOutput);
                    //Render the response to the Browser
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=OppertunityProgress.xlsx");
                    Response.BinaryWrite(ep);
                    Response.End();
                }

            }

            //Render the Page
            eWare.AddContent(container.Execute());
            Response.Write(eWare.GetPage());

        }

        private String getRelevantOppsSQL(DateTime previousDate, DateTime currentDate)
        {
            //Select statement to find opportunities that were in progress at the time and that are current in progress (looking at the live record)
            var sSQL = "SELECT Company.comp_companyid, Company.comp_name, OpportunityProgress.Oppo_OpportunityId, Oppo_OpportunityProgressId, OpportunityProgress.Oppo_Forecast, OpportunityProgress.Oppo_Status, OpportunityProgress.Oppo_Stage, OpportunityProgress.oppo_projectstartdate, OpportunityProgress.oppo_projectenddate, OpportunityProgress.Oppo_Description, OpportunityProgress.oppo_createddate, User_FirstName + ' ' + User_LastName As userName, Oppo_Opened From OpportunityProgress " +
                                        "INNER JOIN Users on User_UserId = Oppo_AssignedUserId " +
                                        "INNER JOIN Opportunity on Opportunity.Oppo_OpportunityId = OpportunityProgress.Oppo_OpportunityId " +
                                        "INNER JOIN Company on Opportunity.Oppo_PrimaryCompanyId = Company.Comp_CompanyId " +
                                        "WHERE OpportunityProgress.Oppo_OpportunityId in ( " +
            "SELECT a.Oppo_OpportunityId FROM (  " +
            "SELECT Oppo_Stage, Oppo_Status, Min(Oppo_CreatedDate) as Created, Oppo_OpportunityId from OpportunityProgress " +
            "where Oppo_Stage like 'Lost%' or Oppo_Stage like 'Won%' " +
            "group by Oppo_OpportunityId, Oppo_Stage, Oppo_Status, Oppo_OpportunityProgressId, Oppo_CreatedDate " +
            "Having Oppo_CreatedDate > '" + previousDate.ToLongDateString() + " 23:59' " +
            ") a " +
            "union " +
            "SELECT b.Oppo_OpportunityId FROM ( " +
            "SELECT Oppo_Stage, Oppo_Status, Oppo_CreatedDate as Created, Oppo_OpportunityId from Opportunity " +
            "where Oppo_Status like 'In Progress%' AND Oppo_CreatedDate < '" + currentDate.ToLongDateString() + " 23:59' " +
            ") b " +
            ")";

            return sSQL;

        }

        private string getCurrentPreviousRecsSQL(String oppList, DateTime currentDate, DateTime previousDate)
        {
            String sSQL = "";

            sSQL = "SELECT Company.comp_companyid, Company.comp_name, OpportunityProgress.Oppo_OpportunityId, Oppo_OpportunityProgressId, OpportunityProgress.Oppo_Forecast, OpportunityProgress.Oppo_Status, OpportunityProgress.Oppo_Stage, OpportunityProgress.oppo_projectstartdate, OpportunityProgress.oppo_projectenddate, OpportunityProgress.Oppo_Description, OpportunityProgress.oppo_createddate, User_FirstName + ' ' + User_LastName As userName, Oppo_Opened, CAST(opportunityprogress.oppo_forecast/curr_rate AS DECIMAL(18,2)) As FinalAmount From OpportunityProgress " +
                                        "INNER JOIN Users on User_UserId = Oppo_AssignedUserId " +
                                        "INNER JOIN Opportunity on Opportunity.Oppo_OpportunityId = OpportunityProgress.Oppo_OpportunityId " +
                                        "INNER JOIN Company on Opportunity.Oppo_PrimaryCompanyId = Company.Comp_CompanyId " +
                                        "INNER JOIN Currency on Opportunityprogress.Oppo_Forecast_CID = Currency.Curr_CurrencyID " +
                                        "WHERE Oppo_OpportunityProgressId in ( " +
                                        "SELECT Oppo_OpportunityProgressId FROM OpportunityProgress AS sod INNER JOIN Users on Oppo_AssignedUserId = Users.User_UserId WHERE (Oppo_OpportunityProgressId IN " +
                                            "(SELECT TOP (1) Oppo_OpportunityProgressId FROM OpportunityProgress " +
                                            "WHERE	 " +
                                                "(Oppo_OpportunityId = sod.Oppo_OpportunityId) AND " +
                                                "(Oppo_CreatedDate < '" + currentDate.ToLongDateString() + " 23:59') " +
                                                "ORDER BY " +
                                                "Oppo_OpportunityProgressId DESC " +
                                                ") " +
                                                ") AND Oppo_Deleted is null AND Oppo_OpportunityId in (" + oppList + ") " +
                                        ") " +

                    "union all " +

                    "SELECT Company.comp_companyid, Company.comp_name, OpportunityProgress.Oppo_OpportunityId, Oppo_OpportunityProgressId, OpportunityProgress.Oppo_Forecast, OpportunityProgress.Oppo_Status, OpportunityProgress.Oppo_Stage, OpportunityProgress.oppo_projectstartdate, OpportunityProgress.oppo_projectenddate, OpportunityProgress.Oppo_Description, OpportunityProgress.oppo_createddate, User_FirstName + ' ' + User_LastName As userName, Oppo_Opened,  CAST(opportunityprogress.oppo_forecast/curr_rate AS DECIMAL(18,2)) As FinalAmount From OpportunityProgress " +
                                        "INNER JOIN Users on User_UserId = Oppo_AssignedUserId " +
                                        "INNER JOIN Opportunity on Opportunity.Oppo_OpportunityId = OpportunityProgress.Oppo_OpportunityId " +
                                        "INNER JOIN Company on Opportunity.Oppo_PrimaryCompanyId = Company.Comp_CompanyId " +
                                        "INNER JOIN Currency on Opportunityprogress.Oppo_Forecast_CID = Currency.Curr_CurrencyID " +
                                        "WHERE Oppo_OpportunityProgressId in ( " +
                                        "SELECT Oppo_OpportunityProgressId FROM OpportunityProgress AS sod INNER JOIN Users on Oppo_AssignedUserId = Users.User_UserId WHERE (Oppo_OpportunityProgressId IN " +
                                            "(SELECT TOP (1) Oppo_OpportunityProgressId FROM OpportunityProgress " +
                                            "WHERE " +	 
                                                "(Oppo_OpportunityId = sod.Oppo_OpportunityId) AND " +
                                                "(Oppo_CreatedDate < '" + previousDate.ToLongDateString() + " 23:59') " +
                                                "ORDER BY " +
                                                "Oppo_OpportunityProgressId DESC " +
                                                ") " +
                                                ") AND Oppo_Deleted is null AND Oppo_OpportunityId in (" + oppList + ") " +
                                        ")" +
                                        " Order By OpportunityProgress.oppo_createddate desc"; 

            return sSQL;
        }
        
       

        private ArrayList buildParameters(String DateFrom, String DateTo, String revision)
        {
            ArrayList contArry = new ArrayList();
            contArry.Add("Current Report Date");
            contArry.Add("<input type='Text' id='iCurrentDate' name='currentDate' class='datepicker' value='" + DateFrom + "'/>");
            contArry.Add("Previous Report Date");
            contArry.Add("<input type='Text' id='iPreviousDate' name='previousDate' class='datepicker' value='" + DateTo + "'/>");
            contArry.Add("Output Method");
            contArry.Add("<Select id='outputMethod' name='outputMethod' style='width: 200px'><option value='1'>Render results on Screen</option><option value='2'>Export to Spreadsheet</option>");
            contArry.Add("Output Format");
            contArry.Add("<Select id='formatSpreadSheet' name='formatSpreadSheet' style='width: 200px'><option value='1'>Format Output</option><option value='2'>No Formatting Applied</option>");
            contArry.Add("Only Show Changes?");
            contArry.Add("<input type='checkbox' name='chkChangesOnly' value='1'/>");
            return contArry;      
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }

        }




    }






}