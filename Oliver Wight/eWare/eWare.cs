﻿using eWare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Oliver_Wight.eWare
{

    public class eWareConnector
    {
        // Static values for button types
        public Int32 Button_Default = 1, Button_Delete = 2, Button_Continue = 4;

        /// <summary>
        /// Standard Init function. KeepHistory an optional parameter for when calling from AJAX etc.
        /// </summary>
        /// <param name="KeepHistory">Optional parameter to not remember visiting e.g. if AJAX call.</param>
        /// <returns></returns>
        public eWareBase Init(bool KeepHistory = false)
        {
            string InstallName = getInstallName(HttpContext.Current.Request.ServerVariables["URL"]);
            string crmProgID = "eWare." + InstallName;
            eWareBase eWare = (eWareBase)Activator.CreateInstance(Type.GetTypeFromProgID(crmProgID));
            string QueryString = HttpContext.Current.Request.Url.Query;
            string HTTPS = HttpContext.Current.Request.ServerVariables["HTTPS"];
            string ServerName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            string Agent = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            string Accept = HttpContext.Current.Request.ServerVariables["HTTP_ACCEPT"];
            eWare.Init(QueryString, "", HTTPS, ServerName, KeepHistory, Agent, Accept);
            return eWare;
        }

        /// <summary>
        /// Standard method for back end logon.
        /// </summary>
        /// <param name="InstallName">CRM install name e.g. CRM</param>
        /// <param name="Username">CRM Username</param>
        /// <param name="Password">CRM Password</param>
        /// <returns></returns>
        public eWareBase Logon(string InstallName, string Username, string Password)
        {
            string crmProgID = "eWare." + InstallName;
            eWareBase eWare = (eWareBase)Activator.CreateInstance(Type.GetTypeFromProgID(crmProgID));
            eWare.Logon(Username, Password);
            return eWare;
        }

        /// <summary>
        /// Function which gets the CRM install name from the QueryString
        /// </summary>
        /// <param name="Path">Path is optional, if not supplied is set to HttpContext.Current.Request.ServerVariables["URL"]</param>
        /// <returns></returns>
        public string getInstallName(string Path = "")
        {
            if (Path == "")
                Path = HttpContext.Current.Request.ServerVariables["URL"];
            string InstallName = "";
            int iEndChar = 0;
            int iStartChar = 0;
            Path = Path.ToLower();
            iEndChar = Path.IndexOf("/custompages");
            if (iEndChar == -1)
                iEndChar = Path.IndexOf("/landingpage");
            if (iEndChar != -1)
            {
                iStartChar = Path.Substring(0, iEndChar).LastIndexOf("/");
                iStartChar++;
                InstallName = Path.Substring(iStartChar, iEndChar - 1);
            }
            return InstallName;
        }
    }

}